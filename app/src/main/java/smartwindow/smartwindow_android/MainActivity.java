package smartwindow.smartwindow_android;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    // for debugging
    private static final String TAG = "MainActivity";

    // Context
    private final Context context = MainActivity.this;
    private final Activity activity = MainActivity.this;

    // Request code
    private final int MY_PERMISSION_REQUEST_BLUETOOTH = 1;
    private final int MY_PERMISSION_REQUEST_BLUETOOTH_ADMIN = 2;
    private final int MY_PERMISSION_REQUEST_ACCESS_COURSE_LOCATION = 3;

    // for Bluetooth Service
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothService mBluetoothService;

    // Serial Protocol
    private SerialProtocol serialCMD;

    // WindowCTL
    private final int WINDOWCTL_OPEN = 1;
    private final int WINDOWCTL_CLOSE = 0;

    // Layout components
    private MenuItem menuItem_connectionState;
    private ImageView imgWindow;
    private SwitchCompat swMode;
    boolean imgState = false;

    // async task
    private final static int INTERVAL = 1000;   // 1 sec
    private final Handler updateData = new Handler();
    Runnable mHandlerTask = new Runnable() {
        @Override
        public void run() {
            serialCMD.sendCMD(SerialProtocol.SerialProtocol_CMD.REFRESH_DATA);
            mHandler.postDelayed(mHandlerTask, INTERVAL);
        }
    };

    // Sensor state
    private class sensorState {
        private static final int NUM_SENSOR = 3;

        public static final int SENSOR_RAINDROP = 0;
        public static final int SENSOR_DUST = 1;
        public static final int SENSOR_GAS = 2;

        private LinearLayoutCompat[] sensorLayout = new LinearLayoutCompat[NUM_SENSOR];
        private AppCompatTextView[] sensorLabel = new AppCompatTextView[NUM_SENSOR];
        private ImageView[] sensorIcon = new ImageView[NUM_SENSOR];
        private AppCompatTextView[] sensorState = new AppCompatTextView[NUM_SENSOR];

        public sensorState(int[] sensorLayoutID, int[] sensorLabelID, int[] sensorImageViewID, int[] sensorStateID) {
            for (int i = 0; i < NUM_SENSOR; i++) {
                sensorLayout[i] = (LinearLayoutCompat) findViewById(sensorLayoutID[i]);
                sensorLabel[i] = (AppCompatTextView) findViewById(sensorLabelID[i]);
                sensorIcon[i] = (ImageView) findViewById(sensorImageViewID[i]);
                sensorState[i] = (AppCompatTextView) findViewById(sensorStateID[i]);
            }
        }

        public void setOnClickListener(int index, View.OnClickListener listener) {
            sensorLayout[index].setOnClickListener(listener);
        }

        public void setImageResource(int index, int drawable) {
            sensorIcon[index].setImageResource(drawable);
        }

        public void setLabelText(int index, String text) {
            sensorLabel[index].setText(text);
        }

        public void setStateText(int index, String text) {
            sensorState[index].setText(text);
        }
    }
    sensorState SensorState;

    // for SwipeRefreshLayout
    private SwipeRefreshLayout RefreshLayout;

    /**
     * The Handler that gets information back from the BluetoothService
     *
     * 메모리 누수가 있을 수 있다고 해요. 그래서 아래처럼 만들어야 한다네요.
     * reference http://maenan.tistory.com/27
     */
    private final Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case BluetoothService.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            Log.d(TAG, "BluetoothService STATE_CONNECTED received");
                            mBluetoothService.write("\r\nConnection Successful!\r\n".getBytes());

                            /*
                             * Start update
                             */
                            mHandlerTask.run();

                            menuItem_connectionState.setTitle(getResources().getString(R.string.con_connected));
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            Log.d(TAG, "BluetoothService STATE_CONNETING received");
                            menuItem_connectionState.setTitle(getResources().getString(R.string.con_connecting));
                            break;
                        case BluetoothService.STATE_LISTEN:
                            Log.d(TAG, "BluetoothService STATE_LISTEN received");
                        case BluetoothService.STATE_NONE:
                            Log.d(TAG, "BluetoothService STATE_NONE received");
                            menuItem_connectionState.setTitle(getResources().getString(R.string.con_notconnected));
                            break;
                    }
                    break;
                case BluetoothService.MESSAGE_WRITE:
                    Log.d(TAG, "BluetoothService Message Write received");

                    byte[] writeBuf = (byte[]) msg.obj;

                    // Construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    Log.d(TAG, "WriteMesssage: " + writeMessage);

                    break;
                case BluetoothService.MESSAGE_READ:
                    Log.d(TAG, "BluetoothService Message Read received");

                    byte[] readBuf = (byte[]) msg.obj;

                    for (int i = 0; i < msg.arg1; i++) {
                        Log.d(TAG, "readBuff: " + readBuf[i]);
                        serialCMD.readCMD(readBuf[i]);
                    }

                    break;
            }
            return false;
        }
    });
    // msp Handler
    private final Handler mspHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch(msg.what){
                case SerialProtocol.MSP_MESSAGE_RECEIVED:
                    SerialProtocol.mspData _mspData = (SerialProtocol.mspData)msg.obj;

                    byte _command = _mspData.getCommand();
                    int command;

                    if (_command < 0) {
                        command = (int)_command + 256;
                    } else {
                        command = (int)_command;
                    }

                    Log.d(TAG, "Serial MSP message received. cmd: " + _mspData.getCommand());

                    if (command == SerialProtocol.SerialProtocol_CMD.WINDOWCTL_OPEN.getValue()) {
                        Log.d(TAG, "SerialProtocol WINDOWCTL_OPEN Received");

                        windowCTL_open();
                    } else if (command == SerialProtocol.SerialProtocol_CMD.WINDOWCTL_CLOSE.getValue()) {
                        Log.d(TAG, "SerialProtocol WINDOWCTL_CLOSE Received");

                        windowCTL_close();
                    } else if (command == SerialProtocol.SerialProtocol_CMD.SENSOR_RAINDROP_STATUS.getValue()) {
                        Log.d(TAG, "SerialProtocol RAINDROP_STATUS Received");
                        if (_mspData.getData()[0] > 0) {
                            SensorState.setImageResource(sensorState.SENSOR_RAINDROP, R.drawable.icon_weather_rain);
                            SensorState.setStateText(sensorState.SENSOR_RAINDROP, "우천");
                        } else {
                            SensorState.setImageResource(sensorState.SENSOR_RAINDROP, R.drawable.icon_weather_no_rain);
                            SensorState.setStateText(sensorState.SENSOR_RAINDROP, "무강우");
                        }

                    } else if (command == SerialProtocol.SerialProtocol_CMD.SENSOR_DUST_STATUS.getValue()) {
                        Log.d(TAG, "SerialProtocol DUST_STATUS Received");
                        if (_mspData.getData()[0] > 0) {
                            SensorState.setImageResource(sensorState.SENSOR_DUST, R.drawable.icon_dust_critical);
                            SensorState.setStateText(sensorState.SENSOR_DUST, "주의보");
                        } else {
                            SensorState.setImageResource(sensorState.SENSOR_DUST, R.drawable.icon_dust_critical);
                            SensorState.setStateText(sensorState.SENSOR_DUST, "정상");
                        }

                    } else if (command == SerialProtocol.SerialProtocol_CMD.SENSOR_GAS_STATUS.getValue()) {
                        Log.d(TAG, "SerialProtocol GAS_STATUS Received");
                        if (_mspData.getData()[0] > 0) {
                            SensorState.setImageResource(sensorState.SENSOR_GAS, R.drawable.icon_gas_leak);
                            SensorState.setStateText(sensorState.SENSOR_GAS, "가스누출");
                        } else {
                            SensorState.setImageResource(sensorState.SENSOR_GAS, R.drawable.icon_gas_normal);
                            SensorState.setStateText(sensorState.SENSOR_GAS, "정상");
                        }

                    } else if (command == SerialProtocol.SerialProtocol_CMD.REFRESH_DATA.getValue()) {
                        Log.d(TAG, "SerialProtocol REFRESH_DATA Received");

                        final int WINDOWCTL_MODE = 0;
                        final int WINDOWSTATUS = 1;
                        final int RAINDROP_STATUS = 2;
                        final int DUST_STATUS = 3;

                        byte[] _data = _mspData.getData();
                        Log.d(TAG, "SerialProtocol DATA SIZE: " + _data.length);
                        Log.d(TAG, "SerialProtocol WINDOWSTATUS: " + _data[WINDOWSTATUS]);

                        if (_data[WINDOWCTL_MODE] == 1) {
                            swMode.setChecked(true);
                        } else {
                            swMode.setChecked(false);
                        }

                        if (_data[WINDOWSTATUS] == 1) {
                            windowCTL_open();
                        } else {
                            windowCTL_close();
                        }

                        Log.d(TAG, "SerialProtocol RAINDROP STATUS: " + _data[RAINDROP_STATUS]);
                        if (_data[RAINDROP_STATUS] > 0) {
                            SensorState.setImageResource(sensorState.SENSOR_RAINDROP, R.drawable.icon_weather_rain);
                            SensorState.setStateText(sensorState.SENSOR_RAINDROP, "비");
                        } else {
                            SensorState.setImageResource(sensorState.SENSOR_RAINDROP, R.drawable.icon_weather_no_rain);
                            SensorState.setStateText(sensorState.SENSOR_RAINDROP, "맑음");
                        }

                        Log.d(TAG, "SerialProtocol DUST STATUS: " + _data[DUST_STATUS]);
                        if (_data[DUST_STATUS] > 0) {
                            SensorState.setImageResource(sensorState.SENSOR_DUST, R.drawable.icon_dust_critical);
                            SensorState.setStateText(sensorState.SENSOR_DUST, "주의보");
                        } else {
                            SensorState.setImageResource(sensorState.SENSOR_DUST, R.drawable.icon_dust_normal);
                            SensorState.setStateText(sensorState.SENSOR_DUST, "정상");
                        }
                    }

                    break;
            }

            return false;
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*
         * Check permissions
         */
        String PERMISSION_STRING = Manifest.permission.BLUETOOTH;
        if (ContextCompat.checkSelfPermission(context, PERMISSION_STRING) !=
                PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    PERMISSION_STRING)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{PERMISSION_STRING}, MY_PERMISSION_REQUEST_BLUETOOTH);
            }
        } else {
            Log.d(TAG, "android.permission.BLUETOOTH was granted!");
        }

        PERMISSION_STRING = Manifest.permission.BLUETOOTH_ADMIN;
        if (ContextCompat.checkSelfPermission(context, PERMISSION_STRING) !=
                PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    PERMISSION_STRING)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{PERMISSION_STRING}, MY_PERMISSION_REQUEST_BLUETOOTH_ADMIN);
            }
        } else {
            Log.d(TAG, "android.permission.BLUETOOTH_ADMIN was granted!");
        }

        PERMISSION_STRING = Manifest.permission.ACCESS_COARSE_LOCATION;
        if (ContextCompat.checkSelfPermission(context, PERMISSION_STRING) !=
                PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    PERMISSION_STRING)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{PERMISSION_STRING}, MY_PERMISSION_REQUEST_ACCESS_COURSE_LOCATION);
            }
        } else {
            Log.d(TAG, "android.permission.ACCESS_COURSE_LOCATION was granted!");
        }

        /*
         * UI elements
         */
        // Basic elements
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.main_drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.main_drawer_open_desc, R.string.main_drawer_close_desc);

        // drawerLayout.setDrawerListener is deprecated.
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        // item
        imgWindow = (ImageView) findViewById(R.id.main_ivWindowState);
        imgWindow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBluetoothService.getState() == BluetoothService.STATE_CONNECTED) {
                    if (windowCTL_isWindowOpen()) {
                        serialCMD.sendCMD(SerialProtocol.SerialProtocol_CMD.WINDOWCTL_CLOSE);
                    } else {
                        serialCMD.sendCMD(SerialProtocol.SerialProtocol_CMD.WINDOWCTL_OPEN);
                    }

                } else {
                    Toast.makeText(MainActivity.this, R.string.error_BTconnectionRequire, Toast.LENGTH_SHORT).show();
                }
            }
        });
        swMode = (SwitchCompat) findViewById(R.id.switchAuto);
        swMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                byte[] _data;

                if (b) {
                    _data = new byte[1];
                    _data[0] = 1;
                    serialCMD.sendCMD(SerialProtocol.SerialProtocol_CMD.WINDOWCTL_SET_MODE, _data);
                } else {
                    _data = new byte[1];
                    _data[0] = 0;
                    serialCMD.sendCMD(SerialProtocol.SerialProtocol_CMD.WINDOWCTL_SET_MODE, _data);
                }
            }
        });

        // SensorState
        int[] sensorLayoutID = {R.id.linearLayoutRainDrop, R.id.linearLayoutDust, R.id.linearLayoutGas};
        int[] sensorTextLabelID = {R.id.txtRainDropLabel, R.id.txtDustLabel, R.id.txtGasLabel};
        int[] sensorIconID = {R.id.imgRainDrop, R.id.imgDust, R.id.imgGas};
        int[] sensorContentsID = {R.id.txtRainDropContent, R.id.txtDustContent, R.id.txtGasContent};
        SensorState = new sensorState(sensorLayoutID, sensorTextLabelID, sensorIconID, sensorContentsID);

        View.OnClickListener rainDropClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serialCMD.sendCMD(SerialProtocol.SerialProtocol_CMD.SENSOR_RAINDROP_STATUS);
            }
        };

        View.OnClickListener dustClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serialCMD.sendCMD(SerialProtocol.SerialProtocol_CMD.SENSOR_DUST_STATUS);
            }
        };

        View.OnClickListener gasClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serialCMD.sendCMD(SerialProtocol.SerialProtocol_CMD.SENSOR_GAS_STATUS);
            }
        };

        SensorState.setOnClickListener(SensorState.SENSOR_RAINDROP, rainDropClick);
        SensorState.setOnClickListener(SensorState.SENSOR_DUST, dustClick);
        SensorState.setOnClickListener(SensorState.SENSOR_GAS, gasClick);

        // SwipeRefreshLayout
        RefreshLayout = (SwipeRefreshLayout) findViewById(R.id.RefreshLayout);

        /*
         * Bluetooth
         */
        BluetoothManager mBluetoothManager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        mBluetoothService = BluetoothService.newInstance(MainActivity.this, mBluetoothAdapter, mHandler);

        /*
         * Serial protocol
         */
        serialCMD = SerialProtocol.newInstance(mspHandler, mBluetoothService);

        /*
         * 리프레시
         */
        RefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mBluetoothService.getState() == BluetoothService.STATE_CONNECTED) {
                    serialCMD.sendCMD(SerialProtocol.SerialProtocol_CMD.REFRESH_DATA);

                } else {
                    Toast.makeText(MainActivity.this, R.string.error_BTconnectionRequire, Toast.LENGTH_SHORT).show();
                }



                RefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_actionmenu, menu);

        menuItem_connectionState = menu.findItem(R.id.main_action_connect);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_action_connect:
                // menuItem_connectionState pressed.
                if (mBluetoothService.getState() == BluetoothService.STATE_CONNECTED) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setTitle("연결 상태");
                    alertDialogBuilder.setMessage("연결된 기기 정보: " + mBluetoothService.getConnectedDeviceInfo());
                    alertDialogBuilder.setPositiveButton("연결 해제", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mBluetoothService.write("\r\nDisconnected".getBytes());
                            mBluetoothService.stop();
                        }
                    });
                    alertDialogBuilder.setNegativeButton("취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                } else {
                    if (mBluetoothAdapter == null) {
                        Toast.makeText(MainActivity.this, R.string.error_noBtDevice, Toast.LENGTH_SHORT).show();
                    } else {
                        showBluetoothDialog();
                    }
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * 블루투스 선택 다이얼로그를 불러오는 메소드
     */
    private void showBluetoothDialog() {
        BluetoothDialogFragment dialogFragment;

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        ArrayList<String> pairedDevicesArray = new ArrayList<>();

        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                pairedDevicesArray.add(device.getName() + "\n" + device.getAddress());
            }
        }

        dialogFragment = BluetoothDialogFragment.newInstance(R.string.dialog_bt_title, mBluetoothAdapter, pairedDevicesArray);
        dialogFragment.show(getSupportFragmentManager(), "dialog_bt");
    }

    /**
     * DialogFragment로 부터 선택된 정보를 받기 위한 메소드
     *
     * @param item 연결할 블루투스 디바이스 정보
     */
    public void onUserSelectItem(String item) {
        String[] deviceinfo = item.split("\n");
        Log.d(TAG, "Selected Deviceinfo: " + deviceinfo[0] + " " + deviceinfo[1]);
        BluetoothDevice mDevice = mBluetoothAdapter.getRemoteDevice(deviceinfo[1]);

        mBluetoothService.connect(mDevice);
    }

    /**
     * 창문 열림
     */
    private void windowCTL_open() {
        imgState = true;
        imgWindow.setImageResource(R.drawable.img_openwindow);
    }

    /**
     * 창문 닫힘
     */
    private void windowCTL_close() {
        imgState = false;
        imgWindow.setImageResource(R.drawable.img_closewindow);
    }

    private boolean windowCTL_isWindowOpen() {
        return imgState;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Stop update
        updateData.removeCallbacks(mHandlerTask);
        
        // 모든 연결 종료
        mBluetoothService.stop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_BLUETOOTH:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                    Log.d(TAG, "android.permission.BLUETOOTH was granted!");
                } else {
                    // permission denied, boo!
                    Log.d(TAG, "android.permission.BLUETOOTH was denied!");

                    new AlertDialog.Builder(context)
                            .setTitle("Permission Denied!")
                            .setMessage("이 앱을 사용하려면 BLUETOOTH 퍼미션이 필요합니다.")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // continue with delete
                                    String PERMISSION_STRING = Manifest.permission.BLUETOOTH;
                                    if (ContextCompat.checkSelfPermission(context, PERMISSION_STRING) !=
                                            PackageManager.PERMISSION_GRANTED) {
                                        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                                PERMISSION_STRING)) {
                                            // Show an explanation to the user *asynchronously* -- don't block
                                            // this thread waiting for the user's response! After the user
                                            // sees the explanation, try again to request the permission.

                                        } else {
                                            ActivityCompat.requestPermissions(activity,
                                                    new String[]{PERMISSION_STRING}, MY_PERMISSION_REQUEST_BLUETOOTH);
                                        }
                                    } else {
                                        Log.d(TAG, "android.permission.BLUETOOTH was granted!");
                                    }

                                    dialogInterface.dismiss();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                    finish();
                                }
                            })
                            .show();
                }
                break;
            case MY_PERMISSION_REQUEST_BLUETOOTH_ADMIN:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                    Log.d(TAG, "android.permission.BLUETOOTH_ADMIN was granted!");
                } else {
                    // permission denied, boo!
                    Log.d(TAG, "android.permission.BLUETOOTH_ADMIN was denied!");

                    new AlertDialog.Builder(context)
                            .setTitle("Permission Denied!")
                            .setMessage("이 앱을 사용하려면 BLUETOOTH_ADMIN 퍼미션이 필요합니다.")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // continue with delete
                                    String PERMISSION_STRING = Manifest.permission.BLUETOOTH_ADMIN;
                                    if (ContextCompat.checkSelfPermission(context, PERMISSION_STRING) !=
                                            PackageManager.PERMISSION_GRANTED) {
                                        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                                PERMISSION_STRING)) {

                                            // Show an explanation to the user *asynchronously* -- don't block
                                            // this thread waiting for the user's response! After the user
                                            // sees the explanation, try again to request the permission.
                                        } else {
                                            ActivityCompat.requestPermissions(activity,
                                                    new String[]{PERMISSION_STRING}, MY_PERMISSION_REQUEST_BLUETOOTH_ADMIN);
                                        }
                                    } else {
                                        Log.d(TAG, "android.permission.BLUETOOTH_ADMIN was granted!");
                                    }

                                    dialogInterface.dismiss();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                    finish();
                                }
                            })
                            .show();
                }
                break;
            case MY_PERMISSION_REQUEST_ACCESS_COURSE_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                    Log.d(TAG, "android.permission.ACCESS_COURSE_LOCATION was granted!");

                } else {
                    // permission denied, boo!
                    Log.d(TAG, "android.permission.ACCESS_COURSE_LOCATION was denied!");

                    new AlertDialog.Builder(context)
                            .setTitle("Permission Denied!")
                            .setMessage("이 앱을 사용하려면 COURSE LOCATION 퍼미션이 필요합니다.")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // continue with delete
                                    String PERMISSION_STRING = Manifest.permission.ACCESS_COARSE_LOCATION;
                                    if (ContextCompat.checkSelfPermission(context, PERMISSION_STRING) !=
                                            PackageManager.PERMISSION_GRANTED) {
                                        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                                PERMISSION_STRING)) {

                                            // Show an explanation to the user *asynchronously* -- don't block
                                            // this thread waiting for the user's response! After the user
                                            // sees the explanation, try again to request the permission.
                                        } else {
                                            ActivityCompat.requestPermissions(activity,
                                                    new String[]{PERMISSION_STRING}, MY_PERMISSION_REQUEST_ACCESS_COURSE_LOCATION);
                                        }
                                    } else {
                                        Log.d(TAG, "android.permission.ACCESS_COURSE_LOCATION was granted!");
                                    }

                                    dialogInterface.dismiss();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                    finish();
                                }
                            })
                            .show();
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}