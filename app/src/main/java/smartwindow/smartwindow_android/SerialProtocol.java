package smartwindow.smartwindow_android;

import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import android.os.Handler;

/**
 * Created by SeoJu on 2016-08-20.
 */
public class SerialProtocol {
    // For debugging TAG
    private static final String TAG = "SerialProtocol";

    // Preamble
    //private static final String PREAMBLE = "$S";
    private static final Byte[] PREAMBLE = {'$', 'M'};

    private SerialProtocol_STATUS serialStatus = SerialProtocol_STATUS.IDLE;

    private static final int MSP_IN_BUF_SIZE = 64;
    private static final int MSP_OUT_BUF_SIZE = 256;

    private final Handler mHandler;
    private final BluetoothService mBluetoothService;

    public static final int MSP_MESSAGE_RECEIVED = 1;

    public class mspData {
        private byte _command;
        private byte[] _data;

        public mspData() {
        }

        public void initData(int size) {
            _data = new byte[size];
        }

        public int setData(int index, byte data) {
            try {
                if((_data != null) && (index < _data.length)) {
                    _data[index] = data;

                    return 0;
                }
            } catch (NullPointerException e) {}

            return -1;
        }

        public void setCommand(byte cmd){
            _command = cmd;
        }
        public byte[] getData() {
            return _data;
        }

        public byte getCommand() {
            return _command;
        }
    }

    private mspData _mspData = new mspData();
    private int buffCursor = 0;
    private int _size = 0;

    // Command ENUM
    // reference: http://stackoverflow.com/questions/1067352/can-set-enum-start-value-in-java
    public enum SerialProtocol_CMD {
        WINDOWCTL_OPEN(100),
        WINDOWCTL_CLOSE(101),
        WINDOWCTL_STOP(102),

        WINDOWCTL_STATUS(1),
        WINDOWCTL_POS(2),

        WINDOWCTL_MODE(3),
        WINDOWCTL_SET_MODE(4),

        SENSOR_RAINDROP_STATUS(200),
        SENSOR_RAINDROP_RAW(201),
        SENSOR_DUST_STATUS(202),
        SENSOR_DUST_RAW(203),
        SENSOR_GAS_STATUS(204),
        SENSOR_GAS_RAW(205),

        REFRESH_DATA(245);

        private final int id;
        SerialProtocol_CMD(int id) { this.id = id; }
        public int getValue() { return id; }
    }

    private enum SerialProtocol_STATUS {
        IDLE,
        HEADER_START,
        HEADER_M,
        HEADER_DIR,
        HEADER_SIZE,
        CMD,
        DATA
    }

    private SerialProtocol(Handler handler, BluetoothService btService) {
        mHandler = handler;
        mBluetoothService = btService;
    }

    // Sigletone
    public static SerialProtocol newInstance(Handler handler, BluetoothService btService) {
        return new SerialProtocol(handler, btService);
    }

    // basic send command
    public void sendCMD(SerialProtocol_CMD cmd) {
        this.sendCMD(cmd, null);
    }

    public void sendCMD(SerialProtocol_CMD cmd, byte[] data) {
        byte[] writeBuff = new byte[MSP_OUT_BUF_SIZE];
        int buffCursor = 0;

        writeBuff[buffCursor++] = '$';
        writeBuff[buffCursor++] = 'M';
        writeBuff[buffCursor++] = '>';

        if(data != null) {
            writeBuff[buffCursor++] = (byte)data.length;
        } else {
            writeBuff[buffCursor++] = 0;
        }

        writeBuff[buffCursor++] = (byte)cmd.getValue();

        if(data != null) {
            for (byte i : data) {
                writeBuff[buffCursor++] = i;
            }
        }

        writeBuff[buffCursor++] = calcChecksum((byte)cmd.getValue(), data);

        this.write(buffCursor, writeBuff);
    }

    private void write(int size, byte[] buff) {
        byte[] writeBuff = new byte[size];

        for (int i = 0; i < size; i++) {
            writeBuff[i] = buff[i];
        }

        mBluetoothService.write(writeBuff);
    }
    // basic read command
    public void readCMD(byte ch) {
        Log.d(TAG, "readCMD: " + ch);

        switch (serialStatus) {
        case IDLE:
            if (ch == '$') {
                Log.d(TAG, "STATUS IDLE -> HEADER_START " + ch);

                serialStatus = SerialProtocol_STATUS.HEADER_START;
            } else {
                Log.d(TAG, "STATUS IDLE");

                serialStatus = SerialProtocol_STATUS.IDLE;
            }
            break;
        case HEADER_START:
            if (ch == 'M') {
                Log.d(TAG, "STATUS HEADER_START -> HEADER_M " + ch);

                serialStatus = SerialProtocol_STATUS.HEADER_M;
            } else {
                Log.d(TAG, "STATUS HEADER_START -> IDLE " + ch);

                serialStatus = SerialProtocol_STATUS.IDLE;
            }
            break;
        case HEADER_M:
            if ((int)ch == 60) {
                Log.d(TAG, "STATUS HEADER_M -> HEADER_DIR " + ch);

                serialStatus = SerialProtocol_STATUS.HEADER_DIR;
            } else {
                Log.d(TAG, "STATUS HEADER_M -> IDLE " + ch);

                serialStatus = SerialProtocol_STATUS.IDLE;
            }
            break;
        case HEADER_DIR:
            Log.d(TAG, "STATUS HEADER_DIR -> HEADER_SIZE " + ch);

            if ((int)ch < 0) {
                _size = (int)ch + 256;
            } else {
                _size = (int)ch;
            }

            _mspData.initData(_size);
            serialStatus = SerialProtocol_STATUS.HEADER_SIZE;
            break;
        case HEADER_SIZE:
            Log.d(TAG, "STATUS HEADER_SIZE -> CMD " + ch);

            _mspData.setCommand(ch);
            buffCursor = 0;
            serialStatus = SerialProtocol_STATUS.CMD;
            break;
        case CMD:
            Log.d(TAG, "STATUS CMD -> DATA");

            if (buffCursor > MSP_IN_BUF_SIZE) {
                Log.d(TAG, "buffCurser over MSP_IN_BUF_SIZE");
                serialStatus = SerialProtocol_STATUS.IDLE;
                break;
            } else if (_size == 0) {
                Log.d(TAG, "Command size is 0");

                serialStatus = SerialProtocol_STATUS.DATA;
            } else if (buffCursor > (_size - 1)) {
                Log.d(TAG, "buffCurser over _size - 1");

                serialStatus = SerialProtocol_STATUS.DATA;
            } else {
                Log.d(TAG, "Receiving data from serial");

                _mspData.setData(buffCursor++, ch);
                break;
            }

        case DATA:
            if (!verifyChecksum(_mspData.getCommand(), _mspData.getData(), ch)) {
                Log.d(TAG, "Verify Checksum FAILED");
            } else {
                Log.d(TAG, "Command Received!");

                mHandler.obtainMessage(this.MSP_MESSAGE_RECEIVED, _mspData).sendToTarget();
            }

            serialStatus = SerialProtocol_STATUS.IDLE;

            break;
        }
    }

    private byte calcChecksum ( byte cmd, byte[] data) {
        byte checksum = 0;

        if(data != null) {
            checksum ^= data.length;
        } else {
            checksum ^= 0;
        }

        checksum ^= cmd;

        if(data != null) {
            for (byte j : data) {
                checksum ^= j;
            }
        }

        Log.d(TAG, "Checksum: " + checksum);
        return checksum;
    }

    private boolean verifyChecksum (byte cmd, byte[] data, byte checksum) {
        if (calcChecksum(cmd, data) == checksum) {
            return true;
        } else {
            return false;
        }
    }
}
