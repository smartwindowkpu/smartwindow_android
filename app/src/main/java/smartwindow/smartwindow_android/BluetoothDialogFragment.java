package smartwindow.smartwindow_android;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.ListViewCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by SeoJu on 2016-07-26.
 */
public class BluetoothDialogFragment extends AppCompatDialogFragment {
    // For debugging
    private static final String TAG = "BluetoothDialogFragment";

    // Requestcode
    private static final int REQUEST_BT_ENABLE = 1;
    private static final int REQUEST_PERMISSION_COURSE_LOCATION = 2;

    // Argument
    private static final String TITLE_ARG = "title";
    private static final String PAIREDDEVICE_ARG = "pairedDevices";

    // Layout components
    private ListViewCompat lvPairedDevice;
    private ListViewCompat lvSearchedDevice;
    private Button btnStartSearch;
    private ProgressBar pbSearchProgressBar;

    // Listview adapter
    private ArrayAdapter<String> arrayAdapterPairedDevice;
    private ArrayAdapter<String> arrayAdapterSearchedDevice;

    // Bluetooth Adapter
    private BluetoothAdapter mBluetoothAdapter;

    // Bluetooth broadcast receiver
    private BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Broadcast received: " + intent.getAction());

            String intentName = intent.getAction();

            if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(intentName)) {
                btnStartSearch.setEnabled(false);
                pbSearchProgressBar.setVisibility(View.VISIBLE);
                arrayAdapterSearchedDevice.clear();
            } else  if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(intentName)) {
                btnStartSearch.setEnabled(true);
                pbSearchProgressBar.setVisibility(View.GONE);

                if (arrayAdapterSearchedDevice.isEmpty()) {
                    arrayAdapterSearchedDevice.add(
                            getResources().getString(R.string.dialog_bt_noDeviceFound));
                    arrayAdapterSearchedDevice.notifyDataSetChanged();
                    lvSearchedDevice.setEnabled(false);
                }
            } else if (BluetoothDevice.ACTION_FOUND.equals(intentName)) {
                BluetoothDevice searchedDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                arrayAdapterSearchedDevice.add(
                        searchedDevice.getName() + "\n" + searchedDevice.getAddress());
                arrayAdapterSearchedDevice.notifyDataSetChanged();
                lvSearchedDevice.setEnabled(true);
            }
        }
    };

    // Singletone
    public static BluetoothDialogFragment newInstance (int title, BluetoothAdapter mBtAdapter, ArrayList<String> pairedDevices) {
        Log.d(TAG, "BluetoothDialogFragment newInstance");

        BluetoothDialogFragment dialogFragment = new BluetoothDialogFragment();
        dialogFragment.setObjects(mBtAdapter);

        Bundle args =new Bundle();
        args.putInt(TITLE_ARG, title);
        args.putStringArrayList(PAIREDDEVICE_ARG, pairedDevices);

        dialogFragment.setArguments(args);

        return dialogFragment;
    }

    // For assign local objects
    public void setObjects(BluetoothAdapter mBtAdapter) {
        mBluetoothAdapter = mBtAdapter;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Log.d(TAG, "OnCreate");

        int hasPermission;
        int title = getArguments().getInt("title");
        ArrayList<String> pairedDevices = getArguments().getStringArrayList(PAIREDDEVICE_ARG);

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_bluetooth_fragment, null);

        if (mBluetoothAdapter == null) {
            Toast.makeText(getContext(), R.string.error_noBtDevice, Toast.LENGTH_SHORT).show();
        } else if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);

            startActivityForResult(enableBtIntent, REQUEST_BT_ENABLE);
        }

        // Register bluetooth receiver for discovery start and finished.
        // BluetoothDevice.ACTION_FOUND needs ACCESS_COARSE_LOCATION permission
        getActivity().registerReceiver(mBluetoothReceiver,
                new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED));
        getActivity().registerReceiver(mBluetoothReceiver,
                new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));

        hasPermission = ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION);
        if (hasPermission == PackageManager.PERMISSION_GRANTED) {
            getActivity().registerReceiver(mBluetoothReceiver,
                    new IntentFilter(BluetoothDevice.ACTION_FOUND));
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSION_COURSE_LOCATION);
        }


        lvPairedDevice = (ListViewCompat) view.findViewById(R.id.dialog_lvPairedBtlist);
        lvSearchedDevice = (ListViewCompat) view.findViewById(R.id.dialog_lvSearchedBtList);
        btnStartSearch = (Button) view.findViewById(R.id.dialog_btnStartSearch);
        pbSearchProgressBar = (ProgressBar) view.findViewById(R.id.dialog_pbSearchProgressBar);

        arrayAdapterPairedDevice = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);
        arrayAdapterSearchedDevice = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);

        btnStartSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (mBluetoothAdapter.isDiscovering()) {
                    mBluetoothAdapter.cancelDiscovery();
                }

                mBluetoothAdapter.startDiscovery();
                arrayAdapterSearchedDevice.clear();
                pbSearchProgressBar.setVisibility(View.VISIBLE);

                return false;
            }
        });

        // When method invocation may produce java NullPointerException warning occured.
        // try (Object != null && condition) statement not just condition only statement.
        if (pairedDevices != null && pairedDevices.size() > 0) {
            for (String string : pairedDevices) {
                arrayAdapterPairedDevice.add(string);
            }
        }

        lvPairedDevice.setAdapter(arrayAdapterPairedDevice);
        lvPairedDevice.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                MainActivity callingActivity = (MainActivity) getActivity();
                callingActivity.onUserSelectItem(
                        lvPairedDevice.getItemAtPosition(i).toString());
                dismiss();
            }
        });

        lvSearchedDevice.setAdapter(arrayAdapterSearchedDevice);
        lvSearchedDevice.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                MainActivity callingActivity = (MainActivity) getActivity();
                callingActivity.onUserSelectItem(
                        lvSearchedDevice.getItemAtPosition(i).toString());
                dismiss();
            }
        });

        return new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setView(view)
                .create();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult");

        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_BT_ENABLE:
                if (resultCode != Activity.RESULT_OK) {
                    Toast.makeText(getContext(), R.string.error_cancelBtEnable, Toast.LENGTH_SHORT).show();
                    dismiss();
                }
                break;
            case REQUEST_PERMISSION_COURSE_LOCATION:
                if (resultCode == Activity.RESULT_OK) {
                    getActivity().registerReceiver(mBluetoothReceiver,
                            new IntentFilter(BluetoothDevice.ACTION_FOUND));
                } else {
                    Toast.makeText(getContext(), R.string.error_permission, Toast.LENGTH_SHORT).show();
                    btnStartSearch.setEnabled(false);
                }
                break;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }

        try {
            getActivity().unregisterReceiver(mBluetoothReceiver);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "Unable to unregister broadcast receiver", e);
        }
    }
}
