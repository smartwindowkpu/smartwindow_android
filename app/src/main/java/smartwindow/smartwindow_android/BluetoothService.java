package smartwindow.smartwindow_android;

        import android.bluetooth.BluetoothAdapter;
        import android.bluetooth.BluetoothDevice;
        import android.bluetooth.BluetoothServerSocket;
        import android.bluetooth.BluetoothSocket;
        import android.content.Context;
        import android.os.Bundle;
        import android.os.Handler;
        import android.os.Message;
        import android.util.Log;

        import java.io.IOException;
        import java.io.InputStream;
        import java.io.OutputStream;
        import java.util.UUID;

public class BluetoothService {
    // for Debugging
    private static final String TAG = "BluetoothService";

    // Connection NAme
    private static final String NAME = "BluetoothService";

    // RFCOMM Protocol UUID
    private static final UUID UUID_SPP = UUID.fromString(
            "00001101-0000-1000-8000-00805F9B34FB");

    private AcceptThread mAcceptThread;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;

    private int mState;
    // 연결 상태와 관련된 변수
    public static final int STATE_NONE = 0;
    public static final int STATE_LISTEN = 1;
    public static final int STATE_CONNECTING = 2;
    public static final int STATE_CONNECTED = 3;

    private final Context mContext;
    private final BluetoothAdapter mBluetoothAdapter;
    // Handler is not java.util.logging.Handler... Handler is android.os.handler.
    private final Handler mHandler;

    // Message types sent from the BluetoothService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothService Handler
    public static final String DEVICE_NAME = "Device_name";
    public static final String TOAST = "toast";

    /**
     * Constructor. Prepares a new BluetoothService session.
     *
     * @param context The UI Activity Context
     * @param btAdapter BluetoothAdapter
     * @param handler A Handler to send messages back to the UI Activity
     */
    private BluetoothService(Context context, BluetoothAdapter btAdapter, Handler handler) {
        mContext = context;
        mBluetoothAdapter = btAdapter;
        mHandler = handler;
    }

    /**
     * Singletone pattern
     *
     * @param context The UI Activity Context
     * @param btAdapter Bluetooth Adapter
     * @param handler A Handler to send messages back to the UI Activity
     * @return BluetoothService object
     */
    public static BluetoothService newInstance (Context context, BluetoothAdapter btAdapter, Handler handler) {
        return new BluetoothService(context, btAdapter, handler);
    }

    /**
     * Return the current connection state
     *
     * @return current connection state
     */
    public int getState() {
        return mState;
    }

    /**
     * Set the current state of the connection
     *
     * @param state state an integer defining the current connection state
     */
    private synchronized void setState(int state) {
        Log.d(TAG, "setState: " + mState + " > " + state);
        mState = state;

        // Give the new state to the Handler so the UI Activity can update
        mHandler.obtainMessage(BluetoothService.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
    }

    public String getConnectedDeviceInfo() {
        String result = null;

        if (mState == STATE_CONNECTED) {

        }

        return result;
    }

    /**
     * Start the Bluetooth service. Specifically start AcceptThread to begin a
     * session in listening (server) mode. Called by the Activity onResume()
     */
    public synchronized void start() {
        Log.d(TAG, "BluetoothService start");

        cancelConnectThread();
        cancelConnectedThread();

        setState(STATE_LISTEN);

        // Start the thread to listen on a BluetoothServerSocket
        if (mAcceptThread == null) {
            mAcceptThread = new AcceptThread();
            mAcceptThread.start();
        }
    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     *
     * @param device device the BluetoothDevice to connect
     */
    public synchronized void connect(BluetoothDevice device) {
        Log.d(TAG, "Connect to: " + device.getName());

        cancelConnectThread();
        cancelConnectedThread();

        // Start the thread to connect with the given device
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();
        setState(STATE_CONNECTING);
    }

    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     *
     * @param socket The BluetoothSocket on which the connection was made
     * @param device The BluetoothDevice that has been connected
     */
    public synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {
        Log.d(TAG, "Connected!");

        cancelConnectThread();
        cancelConnectedThread();
        cancelAcceptThread();

        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();

        // Send the name of the connected device back to the UI Activity
        Message msg = mHandler.obtainMessage(BluetoothService.MESSAGE_DEVICE_NAME);
        Bundle bundle = new Bundle();
        bundle.putString(BluetoothService.DEVICE_NAME, device.getName());
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        setState(STATE_CONNECTED);
    }

    /**
     * Stop all thread
     */
    public synchronized void stop() {
        Log.d(TAG, "Stop!");

        cancelConnectThread();
        cancelConnectedThread();
        cancelAcceptThread();

        setState(STATE_NONE);
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     *
     * @param out out the bytes to write
     * @see ConnectedThread#write(byte[])
     */
    public void write(byte[] out) {
        // Create temporary object
        ConnectedThread tmp;

        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != STATE_CONNECTED) { return; }
            tmp = mConnectedThread;
        }

        // Perform the write unsynchronized
        tmp.write(out);
    }

    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private void connectionFailed() {
        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(BluetoothService.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(BluetoothService.TOAST, mContext.getResources().getString(R.string.error_connectionFailed));
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        // Start the service over to restart listening mode
        BluetoothService.this.start();
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private void connectionLost() {
        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(BluetoothService.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(BluetoothService.TOAST, mContext.getResources().getString(R.string.error_connectionLost));
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        // Start the service over to restart listening mode
        BluetoothService.this.start();
    }

    private void cancelConnectThread() {
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }
    }

    private void cancelConnectedThread() {
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
    }

    private void cancelAcceptThread() {
        if (mAcceptThread != null) {
            mAcceptThread.cancel();
            mAcceptThread = null;
        }
    }

    /**
     * This thread runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted
     * (or until cancelled).
     */
    private class AcceptThread extends Thread {
        // The local server socket
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            BluetoothServerSocket tmp = null;

            // Create a new listening server socket
            try {
                tmp = mBluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(NAME, UUID_SPP);
            } catch (IOException e) {
                Log.e(TAG, "Socket listen() failed", e);
            }

            mmServerSocket = tmp;
        }

        public void run() {
            Log.d(TAG, "BEGIN mAcceptThread " + this);

            BluetoothSocket socket = null;

            // Listen to the server socket if we're not connected
            while (mState != STATE_CONNECTED) {
                try {
                    // This is blocking call and will only return on a
                    // successful connection or an exception
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    Log.e(TAG, "Socket accept() failed", e);
                    break;
                }

                // If a connection was accepted
                if (socket != null) {
                    synchronized (BluetoothService.this) {
                        switch (mState) {
                            case STATE_LISTEN:
                            case STATE_CONNECTING:
                                // Situation normal. Start the connected thread.
                                connected(socket, socket.getRemoteDevice());
                                break;
                            case STATE_NONE:
                            case STATE_CONNECTED:
                                // Either not ready or already connected. Terminate new socket
                                try {
                                    socket.close();
                                } catch (IOException e) {
                                    Log.e(TAG, "Unable to close unwanted socket", e);
                                }
                                break;
                        }
                    }
                }
            }

            Log.d(TAG, "END mAcceptThread");
        }

        public void cancel() {
            Log.d(TAG, "Socket cancel");

            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of server failed", e);
            }
        }
    }

    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    private class ConnectThread extends Thread {
        // The local socket
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            mmDevice = device;
            BluetoothSocket tmp = null;

            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {
                tmp = device.createInsecureRfcommSocketToServiceRecord(UUID_SPP);
            } catch (IOException e) {
                Log.e(TAG, "Unable to construct ConnectionThread", e);
            }

            mmSocket = tmp;
        }

        public void run() {
            Log.d(TAG, "BEGIN mConnectThread");

            // Always cancel discovery because it will slow down a connection
            if (mBluetoothAdapter.isDiscovering()) {
                mBluetoothAdapter.cancelDiscovery();
            }

            // Make a connection to the BluetoothSocket
            try {
                // This is blocking call and will only return on a
                // successful connection or an exception
                mmSocket.connect();
            } catch (IOException exceptionConnect) {
                try {
                    mmSocket.close();
                } catch (IOException exceptionClose) {
                    Log.e(TAG, "Unable to close socket", exceptionClose);
                }

                connectionFailed();
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (BluetoothService.this) {
                mConnectThread = null;
            }

            // Start the connected thread
            connected(mmSocket, mmDevice);
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Unable to close socket", e);
            }
        }
    }

    /**
     * This thread rns during a connection with a remote device.
     * It handles all incoming and outgoing transmissions.
     */
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInputStream;
        private final OutputStream mmOutputStream;

        public ConnectedThread(BluetoothSocket socket) {
            Log.d(TAG, "Create ConnectedThread");

            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.d(TAG, "Temp sockets are not created");
            }

            mmInputStream = tmpIn;
            mmOutputStream = tmpOut;
        }

        public void run() {
            Log.d(TAG, "Begin mConnectedThread");

            int bytes;
            int availableBytes = 0;

            try{
                Log.d(TAG, "Thread sleep 500ms");
                Thread.sleep(500);
            } catch (InterruptedException e){
                Log.d(TAG, "Failed sleep thread");
            }

            // Keep listening to the InputStream while connected
            while (mState == STATE_CONNECTED) {
                try {
                    // Read from the InputStream
                    availableBytes = mmInputStream.available();

                    if (availableBytes > 0) {
                        byte[] buffer = new byte[availableBytes];
                        
                        bytes = mmInputStream.read(buffer);
                        Log.d(TAG, "mmInputStream read: " + new String(buffer));

                        if (bytes > 0) {
                            // Send the obtained bytes to the UI Activity
                            mHandler.obtainMessage(BluetoothService.MESSAGE_READ, bytes, -1, buffer).sendToTarget();
                        }
                    }
                } catch (IOException e) {
                    Log.d(TAG, "Disconnected", e);
                    connectionLost();

                    // Start the service over to restart listening mode
                    BluetoothService.this.start();
                    break;
                }
            }
        }

        // Write to the connected OutStream.
        public void write(byte[] buffer) {
            try {
                mmOutputStream.write(buffer);

                // Share the sent message back to the UI Activity
                mHandler.obtainMessage(BluetoothService.MESSAGE_WRITE, -1, -1, buffer).sendToTarget();
            } catch (IOException e) {
                Log.d(TAG, "Exception during write", e);
            }
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.d(TAG, "Socket close is failed");
            }
        }
    }
}